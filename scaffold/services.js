
var services = {
  id:1,
  limit:10,
  sort:'desc',
  records:
  {
    id:1,
    title:'Lorem ipsum dolor sit amet, consectetur adipiscing elit',
    description:'Nunc eu velit hendrerit, egestas quam vitae, lacinia augue. Duis vel nibh nec enim finibus laoreet. Sed faucibus nisl at justo ullamcorper fringilla.'
  }
};

services.find = function(services,data){
  var mysql = require('mysql');
  var connection = mysql.createConnection({
    host     : 'localhost',
    user     : 'root',
    password : '',
    database:'test'
  });

  services.sql = 'SELECT * FROM records WHERE records.`id` = ' + services.id;

  connection.query(services.sql, function(err, results){
    if (err){ 
      throw err;
    }

    //console.log(results);

    data = results;

    return data;
  })
}

services.setId = function(req,services){

  if (req.params.id != null){
    services.id = req.params.id;
  }
  return services;
}

services.setLimit = function(req,services){

  if (req.params.limit != null){
    services.limit = req.params.limit;
  }
  return services;
}

services.setSort = function(req,services){

  if (req.params.sort != null){
    services.sort = req.params.sort;
  }
  return services;
}

services.setNext = function(services){
  services.next = 2;
  return services;
}

module.exports = services;