var express = require('express');
var router = express.Router();
var services = require('../services');
var mysql = require('mysql');

var connection = mysql.createConnection({
  host     : 'localhost',
  user     : 'root',
  password : '',
  database:'test'
});

sql = 'SELECT * FROM records';

connection.query(sql, function(err, results){

  if (err){ 
    throw err;
  }

  services.records.id = results[0].id;
  services.records.title = results[0].title;
  services.records.description = results[0].description;


})


/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Form Validation', success: req.session.success, errors: req.session.errors,services:services});
  req.session.errors = null;
});

/* GET create page. */
router.get('/create', function(req, res, next) {
  res.render('create', { title: 'Create', condition: true, records:services.records});
});

/* GET read page. */
router.get('/read', function(req, res, next) {
  res.render('read', { title: 'Read', condition: true, records:services.records,id:services.id,limit:services.limit});
});

/* GET read id page. */
router.get('/read/:id',function(req, res, next){
  res.render('read',{title: 'Read', condition: true, records:services.records,id:services.id,limit:services.limit});
});

/* GET read id page. */
router.get('/read/:id/:limit',function(req, res, next){
  res.render('read',{title: 'Read', condition: true, records:services.records,id:services.id,limit:services.limit});
});

/* GET details id page. */
router.get('/details/:id/:limit',function(req, res, next){
  res.render('details',{title: 'Details', condition: true, records:services.records,id:services.id,limit:services.limit,next:services.next});
});

/* GET update page. */
router.get('/update', function(req, res, next) {
  res.render('update', { title: 'Update', condition: true, records:services.records});
});

/* GET delete page. */
router.get('/delete', function(req, res, next) {
  res.render('delete', { title: 'Delete', condition: true, records:services.records});
});


router.post('/submit',function(req, res, next){
  req.check('email','Invalid email').isEmail();
  req.check('password','Password is invalid').isLength({min: 4}).equals(req.body.confirmPassword);
  var errors = req.validationErrors();
  if(errors){
    req.session.errors = errors;
    req.session.success = false;
  } else {
    req.session.success = true;
  }
  res.redirect('/');
});


module.exports = router;
